# Fumble

Light voice-only implementation of mumble client.

## Dependencies
```shell
sudo apt install libtool autoconf
```

# Async idea

FumbleApp spawns:
- sync audio threads
- tokio runtime with async resampling, recoding 

- on connect spawns also a network coroutine which communicates with audio pipeline

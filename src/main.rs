use app::FumbleApp;
use eframe::NativeOptions;
use egui::Vec2;

mod app;
mod network;
mod opus;
mod resample;
mod sound;
mod util;

fn main() {
    env_logger::init();

    eframe::run_native(
        Box::new(FumbleApp::new()),
        NativeOptions {
            initial_window_size: Some(Vec2::new(400.0, 800.0)),
            drag_and_drop_support: false,
            transparent: false,
            ..Default::default()
        },
    )
}

use futures::{future::ready, SinkExt, Stream, StreamExt};
use log::{error, info, trace, warn};
use mumble_protocol::{
    control::{
        msgs::{self, Ping},
        ClientControlCodec, ControlPacket,
    },
    crypt::ClientCryptState,
    voice::{VoicePacket, VoicePacketDst, VoicePacketPayload},
    Clientbound,
};
use tokio_stream::wrappers::{errors::BroadcastStreamRecvError, BroadcastStream};

use std::{
    convert::{Into, TryInto},
    marker::PhantomData,
    net::{Ipv4Addr, SocketAddr, ToSocketAddrs},
    time::Duration,
};
use tokio::{
    net::{TcpStream, UdpSocket},
    select,
    sync::{broadcast, oneshot},
    time::interval,
};
use tokio_native_tls::TlsConnector;
use tokio_util::{codec::Decoder, udp::UdpFramed};

use crate::sound::OpusSoundChunk;
use crate::util::get_static_runtime;

pub type UserId = <Clientbound as VoicePacketDst>::SessionId;

pub struct MumbleServerConnection {
    cancel_on_drop: broadcast::Sender<()>,
    playback_sender: broadcast::Sender<(OpusSoundChunk, UserId)>,
}

impl Drop for MumbleServerConnection {
    fn drop(&mut self) {
        let _ignore_error = self.cancel_on_drop.send(());
    }
}
impl MumbleServerConnection {
    pub fn connect(
        addr: &str,
        login: &str,
        recording_stream: impl Stream<Item = OpusSoundChunk> + Send + 'static + Unpin,
    ) -> MumbleServerConnection {
        let cancel_sender = broadcast::channel::<()>(1).0;
        let playback_sender = broadcast::channel(5).0;

        get_static_runtime().spawn(run(
            addr.to_owned(),
            login.to_owned(),
            recording_stream,
            playback_sender.clone(),
            cancel_sender.clone(),
        ));
        MumbleServerConnection {
            cancel_on_drop: cancel_sender,
            playback_sender,
        }
    }

    pub fn get_playback_stream(&self) -> impl Stream<Item = (OpusSoundChunk, UserId)> + Send {
        let rx = self.playback_sender.subscribe();
        BroadcastStream::new(rx)
            .inspect(|sound_chunk| match sound_chunk {
                Ok((chunk, user_id)) => trace!(
                    "Passing {}  frames of sound from connection to mixer pipe of user {}",
                    chunk.len(),
                    user_id
                ),
                Err(BroadcastStreamRecvError::Lagged(..)) => warn!("Playback stream is lagging"),
            })
            .filter_map(|sound_chunk| ready(sound_chunk.ok()))
    }
}

async fn connect(
    server_addr: SocketAddr,
    server_host: String,
    user_name: String,
    accept_invalid_cert: bool,
    crypt_state_sender: oneshot::Sender<ClientCryptState>,
    mut cancellation: broadcast::Receiver<()>,
) {
    // Wrap crypt_state_sender in Option, so we can call it only once
    let mut crypt_state_sender = Some(crypt_state_sender);

    // Connect to server via TCP
    let stream = TcpStream::connect(&server_addr)
        .await
        .expect("Failed to connect to server:");
    println!("TCP connected..");

    // Wrap the connection in TLS
    let mut builder = native_tls::TlsConnector::builder();
    builder.danger_accept_invalid_certs(accept_invalid_cert);
    let connector: TlsConnector = builder
        .build()
        .expect("Failed to create TLS connector")
        .into();
    let tls_stream = connector
        .connect(&server_host, stream)
        .await
        .expect("Failed to connect TLS: {}");
    println!("TLS connected..");

    // Wrap the TLS stream with Mumble's client-side control-channel codec
    let (mut sink, mut stream) = ClientControlCodec::new().framed(tls_stream).split();

    // Handshake (omitting `Version` message for brevity)
    let mut msg = msgs::Authenticate::new();
    msg.set_username(user_name);
    msg.set_opus(true);
    sink.send(msg.into()).await.unwrap();

    println!("Logging in..");
    let mut crypt_state = None;

    // Note: A normal application also has to send periodic Ping packets
    let mut ping_interval = interval(Duration::from_secs(1));

    // Handle incoming packets
    loop {
        select!(
            _ = ping_interval.tick() => {
                sink.send(ControlPacket::Ping(
                    Box::new(Ping::new())
                )).await.unwrap();
            }
            Some(packet) = stream.next() => {
                match packet.unwrap() {
                    ControlPacket::CryptSetup(msg) => {
                        // Wait until we're fully connected before initiating UDP voice
                        crypt_state = Some(ClientCryptState::new_from(
                            msg.get_key()
                                .try_into()
                                .expect("Server sent private key with incorrect size"),
                            msg.get_client_nonce()
                                .try_into()
                                .expect("Server sent client_nonce with incorrect size"),
                            msg.get_server_nonce()
                                .try_into()
                                .expect("Server sent server_nonce with incorrect size"),
                        ));
                    }
                    ControlPacket::ServerSync(_) => {
                        println!("Logged in!");
                        if let Some(sender) = crypt_state_sender.take() {
                            let _ = sender.send(
                                crypt_state
                                    .take()
                                    .expect("Server didn't send us any CryptSetup packet!"),
                            );
                        }
                    }
                    ControlPacket::Reject(msg) => {
                        println!("Login rejected: {:?}", msg);
                    }
                    _ => {}
                }
            }
            _ = cancellation.recv() => {
                return;
            }
        );
    }
}

async fn handle_udp(
    server_addr: SocketAddr,
    crypt_state: ClientCryptState,
    mut recorded_sound: impl Stream<Item = OpusSoundChunk> + Send + Unpin,
    playback_pipe: broadcast::Sender<(OpusSoundChunk, UserId)>,
    mut cancellation: broadcast::Receiver<()>,
) {
    // Bind UDP socket
    let udp_socket = UdpSocket::bind((Ipv4Addr::from(0), 0u16))
        .await
        .expect("Failed to bind UDP socket");
    println!("UDP ready!");

    // Wrap the raw UDP packets in Mumble's crypto and voice codec (CryptState does both)
    let (mut sink, source) = UdpFramed::new(udp_socket, crypt_state).split();
    let drain = source
        .filter_map(|voice_packet| {
            ready(match voice_packet {
                Ok((
                    VoicePacket::Audio {
                        payload: VoicePacketPayload::Opus(data, _eot),
                        session_id,
                        ..
                    },
                    _addr,
                )) => Some((data, session_id)),
                _ => {
                    info!("Dropping packet: {:?}", voice_packet);
                    None
                }
            })
        })
        .for_each(move |opus_chunk| {
            playback_pipe.send(opus_chunk).unwrap();
            ready(())
        });
    get_static_runtime().spawn(drain);

    let mut send_packet_counter = 0;
    loop {
        select!(
            Some(opus) = recorded_sound.next() => {
                send_packet_counter += 1;
                let send_result = sink
                    .send((
                        mumble_protocol::voice::VoicePacket::Audio {
                            _dst: PhantomData,
                            target: 0,
                            session_id: (),
                            seq_num: send_packet_counter,
                            payload: VoicePacketPayload::Opus(opus, false),
                            position_info: None,
                        },
                        server_addr,
                    ))
                    .await;
                if let Err(e) = send_result {
                    error!("Cannot send sound {}", e);
                }
            },
            _ = cancellation.recv() => {
                break;
            }
        );
    }
}

pub async fn run(
    host: String,
    login: String,
    recorded_sound: impl Stream<Item = OpusSoundChunk> + Send + Unpin,
    playback_pipe: broadcast::Sender<(OpusSoundChunk, UserId)>,
    cancellation: broadcast::Sender<()>,
) {
    let server_port = 64738u16;
    let accept_invalid_cert = true;
    let server_addr = (host.as_ref(), server_port)
        .to_socket_addrs()
        .expect("Failed to parse server address")
        .next()
        .expect("Failed to resolve server address");

    // Oneshot channel for setting UDP CryptState from control task
    // FIXME: For simplicity we don't deal with re-syncing, real applications would have to.
    let (crypt_state_sender, crypt_state_receiver) = oneshot::channel::<ClientCryptState>();
    get_static_runtime().spawn(connect(
        server_addr,
        host.to_owned(),
        login.to_owned(),
        accept_invalid_cert,
        crypt_state_sender,
        cancellation.subscribe(),
    ));
    let crypt_state = crypt_state_receiver.await.expect("Cannot get crypt state");

    // Run it

    handle_udp(
        server_addr,
        crypt_state,
        recorded_sound,
        playback_pipe,
        cancellation.subscribe(),
    )
    .await
}

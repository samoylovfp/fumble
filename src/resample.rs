use futures::{Stream, StreamExt};
use rubato::{FftFixedInOut, Resampler};

use crate::{opus::MINIMAL_OPUS_CHUNK_SIZE, sound::RawSound, util::rechunk};

pub fn resample(
    s: impl Stream<Item = RawSound>,
    sample_rate_from: usize,
    sample_rate_to: usize,
) -> impl Stream<Item = RawSound> {
    let mut resampler =
        FftFixedInOut::new(sample_rate_from, sample_rate_to, MINIMAL_OPUS_CHUNK_SIZE, 1);
    rechunk(s, resampler.nbr_frames_needed())
        .map(move |c| resampler.process(&[c]).unwrap().pop().unwrap())
}

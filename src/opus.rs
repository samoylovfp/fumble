use std::collections::HashMap;

use audiopus::coder::{Decoder, Encoder};
use bytes::Bytes;
use futures::{future::ready, Stream, StreamExt};
use log::error;

use crate::{
    network::UserId,
    sound::{DecodedSound, OpusSoundChunk},
    util::rechunk,
};

pub const MINIMAL_OPUS_CHUNK_SIZE: usize = 480;

pub fn encode(s: impl Stream<Item = DecodedSound>) -> impl Stream<Item = OpusSoundChunk> {
    let mut buffer = vec![0; 1 << 12];
    let encoder = Encoder::new(
        audiopus::SampleRate::Hz48000,
        audiopus::Channels::Mono,
        audiopus::Application::Voip,
    )
    .unwrap();

    rechunk(s, MINIMAL_OPUS_CHUNK_SIZE).filter_map(move |c| {
        ready(match encoder.encode(c.as_ref(), &mut buffer) {
            Ok(encoded_len) => Some(Bytes::from(buffer[0..encoded_len].to_owned())),
            Err(e) => {
                // FIXME: deduplicate error messages
                error!("Error encoding sound: {}", e);
                None
            }
        })
    })
}

// FIXME: there seems to be a "float" versions of functions on the opus library
// might use those to avoid integer <-> float conversions
pub fn decode(
    s: impl Stream<Item = (OpusSoundChunk, UserId)>,
) -> impl Stream<Item = (DecodedSound, UserId)> {
    let mut decoders = HashMap::new();
    let mut buffer = vec![0i16; 1 << 12];
    s.filter_map(move |(c, user_id)| {
        let decoder = decoders.entry(user_id).or_insert_with(|| {
            Decoder::new(audiopus::SampleRate::Hz48000, audiopus::Channels::Mono).unwrap()
        });
        ready(match decoder.decode(Some(c.as_ref()), &mut buffer, false) {
            Ok(d) => Some((buffer[0..d].to_owned(), user_id)),
            Err(e) => {
                error!("Error decoding opus: {}", e);
                None
            }
        })
    })
}

use futures::{Stream, StreamExt};
use lazy_static::lazy_static;

use tokio::runtime::Runtime;

pub fn get_static_runtime() -> &'static Runtime {
    lazy_static! {
        static ref RUNTIME: Runtime = Runtime::new().unwrap();
    };
    &RUNTIME
}

pub fn rechunk<T: 'static>(
    s: impl Stream<Item = Vec<T>>,
    size: usize,
) -> impl Stream<Item = Vec<T>> {
    let mut buffer = vec![];
    s.flat_map(move |c| {
        buffer.extend(c);
        let mut result = vec![];
        while buffer.len() >= size {
            let chunk = pop_left(&mut buffer, size);
            result.push(chunk);
        }
        tokio_stream::iter(result)
    })
}

pub fn pop_left<T>(vec: &mut Vec<T>, size: usize) -> Vec<T> {
    let mut remainder = vec.split_off(size);
    std::mem::swap(&mut remainder, vec);
    remainder
}

#[cfg(test)]
mod test {
    use super::*;
    use tokio::sync::mpsc;
    use tokio_stream::wrappers::ReceiverStream;

    #[tokio::test]
    async fn test_rechunk() {
        let (tx, rx) = mpsc::channel::<Vec<u8>>(10);
        let original_stream = ReceiverStream::new(rx);
        let mut stream = rechunk(original_stream, 3);
        let s = |e| tx.try_send(e).unwrap();
        s(vec![1, 2]);
        s(vec![3, 4]);
        assert_eq!(stream.next().await, Some(vec![1, 2, 3]));
        s(vec![1]);
        s(vec![2]);
        assert_eq!(stream.next().await, Some(vec![4, 1, 2]));
        s(vec![6, 6, 6, 6, 6, 6]);
        s(vec![6, 6, 6, 6, 6, 6, 7]);
        drop(tx);
        for _ in 0..4 {
            assert_eq!(stream.next().await, Some(vec![6, 6, 6]));
        }
        assert_eq!(stream.next().await, None);
    }
}

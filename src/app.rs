use std::sync::{atomic::Ordering, Arc};

use eframe::epi::App;
use egui::{CentralPanel, Color32};

use crate::{
    network::MumbleServerConnection,
    sound::{PlaybackPipeline, RecordingPipeline, SoundMixer},
    util::get_static_runtime,
};

pub struct FumbleApp {
    connection: Option<MumbleServerConnection>,
    recording_pipeline: RecordingPipeline,
    _playback_pipeline: PlaybackPipeline,
    mixer: SoundMixer,

    server_address: String,
    login: String,
    current_page: Pages,
}

impl FumbleApp {
    pub fn new() -> Self {
        let mixer = SoundMixer::new();
        let playback = PlaybackPipeline::new(Arc::clone(&mixer.sound));
        Self {
            connection: None,
            recording_pipeline: RecordingPipeline::new(),
            _playback_pipeline: playback,
            mixer,

            server_address: String::new(),
            login: String::new(),
            current_page: Pages::Connection,
        }
    }

    fn connect(&mut self) {
        let conn = MumbleServerConnection::connect(
            &self.server_address,
            &self.login,
            self.recording_pipeline.get_opus_stream(),
        );
        let sound_stream = conn.get_playback_stream();
        self.connection = Some(conn);
        get_static_runtime().spawn(self.mixer.add_to_mix(sound_stream));
    }
}

#[derive(PartialEq, Eq, Debug)]
enum Pages {
    Connection,
    Recording,
    Playback,
}

impl Pages {
    fn variants() -> Vec<Pages> {
        vec![Pages::Connection, Pages::Recording, Pages::Playback]
    }
}

impl App for FumbleApp {
    fn update(&mut self, ctx: &egui::CtxRef, _frame: &mut eframe::epi::Frame<'_>) {
        ctx.set_pixels_per_point(2.0);
        // Force gui re-render to show volume changes
        ctx.request_repaint();

        egui::TopBottomPanel::top("top_nav_bar").show(ctx, |ui| {
            let recording_volume = self.recording_pipeline.level.load(Ordering::Relaxed);
            ui.horizontal(|ui| {
                for p in Pages::variants() {
                    let mut button_color = Color32::from_rgb(0, 0, 0);
                    if p == self.current_page {
                        for i in 0..3 {
                            button_color[i] += 50;
                        }
                    }
                    if p == Pages::Recording {
                        button_color[0] = button_color[0].saturating_add(recording_volume);
                    }
                    let button = egui::Button::new(format!("{:?}", p)).fill(button_color);

                    if ui.add(button).clicked() {
                        self.current_page = p;
                    }
                }
                ui.separator();
            });
        });
        match self.current_page {
            Pages::Connection => {
                CentralPanel::default().show(ctx, |ui| {
                    egui::Grid::new("connect")
                        .min_col_width(60.0)
                        .show(ui, |ui| {
                            ui.label("Address");
                            ui.text_edit_singleline(&mut self.server_address);
                            ui.end_row();

                            ui.label("Login");
                            ui.text_edit_singleline(&mut self.login);
                            ui.end_row();

                            // ui.checkbox(&mut false, "auto-connect");
                            // ui.end_row();
                            let connect_button = ui.add(
                                egui::widgets::Button::new("Connect")
                                    .enabled(self.connection.is_none()),
                            );
                            if connect_button.clicked() {
                                self.connect();
                            }
                            let disconnect_button = ui.add(
                                egui::widgets::Button::new("Disconnect")
                                    .enabled(self.connection.is_some()),
                            );
                            if disconnect_button.clicked() {
                                self.connection = None;
                            }
                            ui.end_row();
                            if self.connection.is_some() {
                                ui.label("Connecting...");
                            }
                        });
                });
            }
            Pages::Playback => { /* Some recording settings */ }
            _ => {}
        };
    }

    fn name(&self) -> &str {
        "Fumble"
    }
}

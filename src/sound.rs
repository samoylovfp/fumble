use std::iter::repeat;
use std::{
    collections::HashMap,
    sync::{atomic::AtomicU8, Arc, Mutex},
};

use bytes::Bytes;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};

use futures::{future::ready, Future, StreamExt};

use log::{error, trace};
use rubato::{FftFixedInOut, Resampler};
use tokio::sync::broadcast::{self};
use tokio_stream::wrappers::BroadcastStream;

use crate::{
    network::UserId,
    opus::{decode, encode, MINIMAL_OPUS_CHUNK_SIZE},
    resample::resample,
    util::pop_left,
};

/// Comes from network
pub type OpusSoundChunk = Bytes;
/// Comes out of opus decoder
pub type DecodedSound = Vec<i16>;
/// Used for resampling and device interactions
pub type RawSound = Vec<f32>;
type SharedIncomingSound = Arc<Mutex<HashMap<UserId, DecodedSound>>>;

const OPUS_SAMPLE_RATE: u32 = 48000;

pub struct PlaybackPipeline {
    _stream: cpal::Stream,
}

pub struct RecordingPipeline {
    _thread: cpal::Stream,
    channel: broadcast::Sender<RawSound>,
    sample_rate: u32,
    pub level: Arc<AtomicU8>,
}

pub struct SoundMixer {
    pub sound: SharedIncomingSound,
}

impl SoundMixer {
    pub fn new() -> Self {
        Self {
            sound: Arc::new(Mutex::new(HashMap::new())),
        }
    }

    pub fn add_to_mix(
        &self,
        stream: impl futures::Stream<Item = (OpusSoundChunk, u32)> + Send,
    ) -> impl Future<Output = ()> {
        let sound_clone = Arc::clone(&self.sound);
        decode(stream).for_each(move |(sound, user_id)| {
            trace!("Putting sound into mixer storage for user {}", user_id);
            sound_clone
                .lock()
                .unwrap()
                .entry(user_id)
                .or_default()
                .extend(sound);
            ready(())
        })
    }
}

impl RecordingPipeline {
    pub fn new() -> Self {
        let channel = broadcast::channel(10).0;
        let level = Arc::new(AtomicU8::new(0));
        let (_thread, sample_rate) = create_record_thread(channel.clone(), Arc::clone(&level));

        Self {
            channel,
            _thread,
            sample_rate,
            level,
        }
    }
    pub fn get_opus_stream(&self) -> impl futures::Stream<Item = OpusSoundChunk> {
        let raw_audio_stream =
            BroadcastStream::new(self.channel.subscribe()).filter_map(|chunk| ready(chunk.ok()));
        let resampled = resample(
            raw_audio_stream,
            self.sample_rate as usize,
            OPUS_SAMPLE_RATE as usize,
        );
        let as_i16 = resampled.map(|c| c.into_iter().map(f32_to_i16).collect());
        encode(as_i16)
    }
}

impl PlaybackPipeline {
    pub fn new(sounds: SharedIncomingSound) -> Self {
        let _stream = create_playback_thread(sounds);
        PlaybackPipeline { _stream }
    }
}

fn f32_to_i16(frame: f32) -> i16 {
    (frame * i16::MAX as f32) as i16
}

fn create_record_thread(
    record_sender: broadcast::Sender<RawSound>,
    level: Arc<AtomicU8>,
) -> (cpal::Stream, u32) {
    let record_device = cpal::default_host()
        .default_input_device()
        .expect("No default input device");
    let record_config = record_device.default_input_config().unwrap().config();
    let record_channels = record_config.channels as usize;
    let record_stream = record_device
        .build_input_stream(
            &record_config,
            move |d: &[f32], _| {
                let max_vol = d
                    .iter()
                    .map(|&f| (f.abs() * 255.0) as u8)
                    .max()
                    .unwrap_or(0);
                level.store(max_vol, std::sync::atomic::Ordering::Relaxed);
                let _ignore_error =
                    record_sender.send(d.iter().copied().step_by(record_channels).collect());
            },
            |e| eprintln!("Error recording: {}", e),
        )
        .unwrap();
    record_stream.play().unwrap();

    (record_stream, record_config.sample_rate.0)
}

fn create_playback_thread(sounds: SharedIncomingSound) -> cpal::Stream {
    let device = cpal::default_host()
        .default_output_device()
        .expect("No default output device");

    let config = device.default_output_config().unwrap().config();
    let sample_rate = config.sample_rate.0;

    let channels = config.channels as usize;
    let mut resampler = FftFixedInOut::new(
        OPUS_SAMPLE_RATE as usize,
        config.sample_rate.0 as usize,
        MINIMAL_OPUS_CHUNK_SIZE,
        1,
    );
    let resampler_chunk = resampler.nbr_frames_needed();

    let mut mixed: RawSound = vec![];
    let mut resampled: RawSound = vec![];

    // TODO: when there is not enough sound for the chunk it might be the user stopped speaking
    // or the packets have not yet arrived.
    // can be distinguished using the "end_of_transmission" network flag

    let stream = device
        .build_output_stream(
            &config,
            move |target: &mut [f32], _| {
                // let started = Instance::now();
                let requested_frames = target.len() / channels;

                for _try in 0..2 {
                    if resampled.len() >= requested_frames {
                        trace!(
                            "Playing {} samples, {} left",
                            requested_frames,
                            resampled.len()
                        );
                        let resampled_chunk = pop_left(&mut resampled, requested_frames);

                        for (resampled_frame, target_frame) in resampled_chunk
                            .into_iter()
                            .flat_map(|c| vec![c; channels]) // Repeat the sample for each channel
                            .zip(target.iter_mut())
                        {
                            *target_frame = resampled_frame;
                        }
                        return;
                    }
                    let mut locked_sounds = sounds.lock().unwrap();
                    trace!(
                        "Mixed buffer is {}, resampled buffer is {}",
                        mixed.len(),
                        resampled.len()
                    );
                    for (k, v) in locked_sounds.iter() {
                        trace!("User {} has {} samples in storage", k, v.len());
                    }

                    for (user_id, user_buffer) in locked_sounds.iter_mut() {
                        if user_buffer.len() >= sample_rate as usize {
                            error!(
                                "User {} accumulated delay more than 1 second, dropping the buffer",
                                user_id
                            );
                            user_buffer.clear();
                        }
                        if user_buffer.len() >= requested_frames {
                            let user_chunk = pop_left(user_buffer, requested_frames);
                            if mixed.len() < user_chunk.len() {
                                let top_up_amount = user_chunk.len().saturating_sub(mixed.len());
                                mixed.extend(repeat(0.0).take(top_up_amount));
                            }
                            for (user_frame, mixed_frame) in
                                user_chunk.into_iter().zip(mixed.iter_mut())
                            {
                                *mixed_frame += user_frame as f32 / i16::MAX as f32;
                                *mixed_frame = mixed_frame.clamp(-1.0, 1.0);
                            }
                        }
                    }
                    while mixed.len() >= resampler_chunk {
                        let mixed_chunk = pop_left(&mut mixed, resampler_chunk);
                        let resampled_chunk =
                            resampler.process(&[mixed_chunk]).unwrap().pop().unwrap();
                        resampled.extend(resampled_chunk);
                    }
                }
                // No sound, proper silence requires zeroing-out
                for device_frame in target.iter_mut() {
                    *device_frame = 0.0;
                }
            },
            |e| eprintln!("Error playing back sound: {}", e),
        )
        .unwrap();
    stream.play().unwrap();
    stream
}

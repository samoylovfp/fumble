- ✔️ Connect via UI
- ✔️ Listen to incoming audio
- ✔️ Multiplex channels when several people speaking
- ✔️ Resample recorded audio

--- MVP ---

- ⌛ Refactor the code so it is more comprehensive
- Re-sync network connection
- Fix audio device not opening on the first try
- Save settings (connected address, login)
- Auto-connect
- Add an icon to the executable
- Select audio devices via UI
- Disconnect via UI
- See what takes up space in the resulting binary (cargo bloat?)
- Joke recorder
- Make Session_id persistent
- Cool graphs for server pings and audio levels
- EQ for individual user